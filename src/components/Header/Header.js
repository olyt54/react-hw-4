import React from 'react';
import NavBar from "../NavBar/NavBar";
import "./Header.scss"

const Header = () => {
    return (
        <header className="header">
            <NavBar/>
        </header>
    );
};

export default Header;