import React from 'react';
import propTypes, {oneOfType} from "prop-types"
import "./button.scss"

const Button = (props) => {
        const {text, bgColor, handleBtn, className} = props;

        return (
            <button className={`btn ${className}`} style={{backgroundColor: bgColor}} onClick={() => handleBtn()}>
                {text}
            </button>
        );
}

Button.propTypes = {
    text: oneOfType([
        propTypes.string,
        propTypes.element
    ]),
    bgColor: propTypes.string,
    handleBtn: propTypes.func
}

export default Button;