import {combineReducers} from "redux";
import goodsReducer from "../goods/goodsReducer";
import favoritesReducer from "../favorites/favoritesReducer";
import {cartReducer} from "../cart/cartReducer";
import addToCartModalReducer from "../addToCartModal/addToCartModalReducer";
import removeFromCartModalReducer from "../removeFromCartModal/removeFromCartModalReducer";


export default combineReducers({
    goods: goodsReducer,
    cart: cartReducer,
    favorites: favoritesReducer,
    isAddToCartModalOpen: addToCartModalReducer,
    isRemoveFromCartModalOpen: removeFromCartModalReducer
})