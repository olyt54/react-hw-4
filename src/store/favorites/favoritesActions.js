import store from "../configStore";
import {UPDATE_FAVORITES} from "./favoritesActionTypes";


export function updateFavorites() {
    return (dispatch) => {

        const goods = store.getState().goods

        if (!localStorage.getItem("favorites")) {
            localStorage.setItem("favorites", JSON.stringify([]))
        }

        const favoritesCodes = JSON.parse(localStorage.getItem("favorites"));
        const newFavorites = goods.filter(item => favoritesCodes.includes(item.vendorCode));

        dispatch({
            type: UPDATE_FAVORITES,
            payload: newFavorites
        })
    }
}