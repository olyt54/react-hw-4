const initialStore = {
    goods: [],
    cart: [],
    favorites: [],
    isAddToCartModalOpen: false,
    isRemoveFromCartModalOpen: false
};

export default initialStore;