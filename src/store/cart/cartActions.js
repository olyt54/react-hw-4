import store from "../configStore";
import {UPDATE_CART} from "./cartActionTypes";


export function updateCart() {
    return (dispatch) => {
        const goods = store.getState().goods

        if (!localStorage.getItem("cart")) {
            localStorage.setItem("cart", JSON.stringify([]))
        }

        const cartCodes = JSON.parse(localStorage.getItem("cart"))
        const newCart = goods.filter(item => cartCodes.includes(item.vendorCode))

        dispatch({
            type: UPDATE_CART,
            payload: newCart
        })
    }
}