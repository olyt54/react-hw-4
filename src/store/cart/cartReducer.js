import initialStore from "../initialStore";
import {UPDATE_CART} from "./cartActionTypes";


export function cartReducer(cartFromStore=initialStore.cart, action) {
    switch (action.type) {
        case UPDATE_CART:
            return action.payload
        default:
            return cartFromStore;
    }
}