export const MODAL_ADD_OPEN = "MODAL_ADD_OPEN";
export const MODAL_ADD_CLOSE = "MODAL_ADD_CLOSE";

export function openAddModal() {
    return (dispatch) => {
        dispatch({
            type: MODAL_ADD_OPEN,
            payload: true
        })
    }
}

export function closeAddModal() {
    return (dispatch) => {
        dispatch({
            type: MODAL_ADD_CLOSE,
            payload: false
        })
    }
}

