export const MODAL_REMOVE_OPEN = "MODAL_REMOVE_OPEN";
export const MODAL_REMOVE_CLOSE = "MODAL_REMOVE_CLOSE";

export function openRemoveModal() {
    return (dispatch) => {
        dispatch({
            type: MODAL_REMOVE_OPEN,
            payload: true
        })
    }
}

export function closeRemoveModal() {
    return (dispatch) => {
        dispatch({
            type: MODAL_REMOVE_CLOSE,
            payload: false
        })
    }
}