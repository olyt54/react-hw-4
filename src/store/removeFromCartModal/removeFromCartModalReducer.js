import initialStore from "../initialStore";
import {MODAL_REMOVE_OPEN, MODAL_REMOVE_CLOSE} from "./removeFromCartModalActions";

export default function addToCartModalReducer(isAddToCartModalOpen = initialStore.isRemoveFromCartModalOpen, action) {
    switch (action.type) {
        case MODAL_REMOVE_OPEN:
            return action.payload;
        case MODAL_REMOVE_CLOSE:
            return action.payload;

        default:
            return isAddToCartModalOpen;
    }
}