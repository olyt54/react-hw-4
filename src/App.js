import React, {useEffect} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {connect} from "react-redux";
import {goodsLoad} from "./store/goods/goodsActions";

const App = (props) => {
    const {goods, loadGoods} = props

    useEffect(() => {
        loadGoods()
    }, [loadGoods])

    return (
        <div className="App">
            <Header/>
            <div className="content-container">
                <AppRoutes goods={goods}/>
            </div>
        </div>
    )
}

const mapStoreToProps = (store) => ({
    goods: store.goods
})

const mapDispatchToProps = (dispatch) => ({
    loadGoods: (goods) => {
        dispatch(goodsLoad(goods))
    }
})

export default connect(mapStoreToProps, mapDispatchToProps)(App)
