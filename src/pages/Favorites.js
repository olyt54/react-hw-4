import React, {useEffect} from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import {connect} from "react-redux";
import {updateFavorites} from "../store/favorites/favoritesActions";


const Favorites = (props) => {
    const {goods, favorites, updateFavorites} = props;

    useEffect(() => {
        updateFavorites()
    }, [goods, updateFavorites]);

    const removeFromFavorites = (vendorCode) => {
        const newFavorites = favorites.filter(i => i.vendorCode !== vendorCode);

        updateFavorites(newFavorites)
    }

    return (
        <>
            <GoodsList goods={favorites} updateFavorites={removeFromFavorites} location={props.location}/>
        </>
    );
};

const mapStoreToProps = (store) => ({
    favorites: store.favorites
})

const mapDispatchToProps = (dispatch) => ({
    updateFavorites: (favorites) => dispatch(updateFavorites(favorites))
})

export default connect(mapStoreToProps, mapDispatchToProps)(Favorites);