import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Home = (props) => {
    const {goods, location} = props;

    return (
        <>
            <GoodsList goods={goods} location={location}/>
        </>
    );
};

export default Home

