import React, {useEffect} from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import {connect} from "react-redux";
import {updateCart} from "../store/cart/cartActions";

const Cart = (props) => {
    const {goods, cart, updateCart, isInCart, location} = props;


    useEffect(() => {
       updateCart();
    }, [goods, updateCart]);

    const removeFromCart = (vendorCode) => {
        const newCart = cart.filter(i => i.vendorCode !== vendorCode);

        updateCart(newCart);
    }

    return (
        <>
            <GoodsList goods={cart} isInCart={isInCart} updateCart={removeFromCart} location={location}/>
        </>
    );
};

const mapStoreToProps = (store) => ({
    cart: store.cart
})

const mapDispatchToProps = (dispatch) => ({
    updateCart: (cart) => dispatch(updateCart(cart))
})

export default connect(mapStoreToProps, mapDispatchToProps)(Cart)

